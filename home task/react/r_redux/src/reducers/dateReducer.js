import { combineReducers } from 'redux';
import { SET_DATE } from '../action/action';

const dateReducer = (state = null, action) => {
    switch (action.type) {
        case SET_DATE:
            return action.payload;
        default:
            return state;
    }
};

const rootReducer = combineReducers({
    date: dateReducer
});

export default rootReducer;