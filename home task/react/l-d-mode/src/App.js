import React, { useState, useEffect } from 'react';
import './App.scss';

function App() {
  const [isDarkMode, setIsDarkMode] = useState(false);

  useEffect(() => {
    if (isDarkMode) {
      document.body.classList.add('dark-mode');
    } else {
      document.body.classList.remove('dark-mode');
    }
  }, [isDarkMode]);

  const toggleDarkMode = () => {
    setIsDarkMode(!isDarkMode);
  };
  return (
    <div className="App">
      <div>
        <button className='b' onClick={toggleDarkMode}>
          {isDarkMode ? 'Switch to Light Mode' : 'Switch to Dark Mode'}
        </button>
        <h1>{isDarkMode ? 'Dark Mode' : 'Light Mode'}</h1>
        <p>{isDarkMode ? 'This is dark mode text.' : 'This is light mode text.'}</p>
      </div>
    </div>
  );
}

export default App;
