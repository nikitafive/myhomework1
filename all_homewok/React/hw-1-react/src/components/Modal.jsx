import React from "react";
import '../style/Modal.scss'
import {Button} from "./Button";

export class Modal extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (

    <modal className='modal' onClick={(ev) =>  this.props.closeButton()}>
    <div className='modal__content'onClick={(ev) => ev.stopPropagation()} >
        <div className='modal__content-title'>
            <h1 className='modal__content-title-text'>{this.props.header}</h1>
            <span className='modal__content-title-close' onClick={() => this.props.closeButton()}>X</span>
        </div>
            <p className='modal__content-text'>{this.props.text}</p>
        <div className='modal__button'>
            {this.props.elements}
        </div>
    </div>
    </modal>
        )
    }
}