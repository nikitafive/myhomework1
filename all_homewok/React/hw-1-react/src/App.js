import React from "react";
import {Button} from './components/Button';
import './style/App.scss';
import {Modal} from './components/Modal';


export class App extends React.Component {
constructor(props) {
    super(props);
    this.state = {
        FirstModal : false,
        SecondModal: false
   }
}

    render() {
        return (
            <div className='App'>
                <Button backgroundColor= '#8B008B' text='Open first modal' onClick={() => this.setState({FirstModal: true})}/>
                <Button backgroundColor= '#9932CC' text='Open second modal' onClick={() => this.setState({SecondModal: true})}/>
                {this.state.FirstModal &&
                    <Modal header = 'You want to delete this file?'
                           text = 'Once deleted, the file cannot be recovered!'
                           closeButton = {() => this.setState({FirstModal: false})}
                           elements = {
                            <>
                             <Button backgroundColor= 'black'
                                    text='Ok'
                                    onClick = {() => this.setState({FirstModal:false})}/>
                                <Button backgroundColor= 'black'
                                        text='Cancel'
                                        onClick = {() => this.setState({FirstModal:false})}/></>}
                    />}
                {this.state.SecondModal &&
                    <Modal header = 'You want to delete this file?'
                           text = 'Once deleted, the file cannot be recovered!'
                           closeButton = {() => this.setState({SecondModal: false})}
                           elements = {
                               <>
                                   <Button backgroundColor= 'black'
                                           text='Ok'
                                           onClick = {() => this.setState({SecondModal:false})}/>
                                   <Button backgroundColor= 'black'
                                           text='Cancel'
                                           onClick = {() => this.setState({SecondModal:false})}/></>}
                    />}
            </div>
        )
    }
}
