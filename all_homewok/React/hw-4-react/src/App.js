import './App.scss';
import React, {useState} from "react";
import ListItem from './components/ListItem/ListItem';
import Header from "./components/Header/Header";

const App = () => {
    const [basket, setBasket] = useState(JSON.parse(localStorage.getItem('basket')) || [])
    const [favorite, setFavorite] = useState(JSON.parse(localStorage.getItem('favorite')) || [])

    function basketUser(item) {
        let repetition = false
        basket.forEach(el => {
            if(el.code === item.code) {
                repetition = true
            }
        })
        if(!repetition) {
            setBasket(basket.concat(item))
            localStorage.setItem('basket', JSON.stringify(basket.concat(item)))
        }
    }

    function deleteItem(item) {
        basket.forEach(el => {
            if (el.code === item.code) {
                setBasket(basket.filter(el => el.code !== item.code))
                localStorage.setItem('basket', JSON.stringify(basket.filter(el => el.code !== item.code)))
            }
        })
    }
    console.log("app", basket)

    function favoriteUser(item) {
        let repetition = false
        favorite.forEach(el => {
            if(el.code === item.code) {
                repetition = true
                setFavorite(favorite.filter(el => el.code !== item.code))
                localStorage.setItem('favorite', JSON.stringify(favorite.filter(el => el.code !== item.code)))
            }
        })
        if(!repetition) {
            setFavorite(favorite.concat(item))
            localStorage.setItem('favorite', JSON.stringify(favorite.concat(item)))
        }
    }

return (
            <div className='App'>
                <Header basket={basket} favorite={favorite} deleteItem={deleteItem} delFav={favoriteUser}/>
                <ListItem basket={basketUser} favorite={favoriteUser} fav={favorite}/>
            </div>
        )

}

export default App;