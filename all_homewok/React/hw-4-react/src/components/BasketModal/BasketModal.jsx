import React from 'react';
import './BasketModal.scss'
import PropTypes from "prop-types";

const BasketModal = (props) => {
    return (
        <div className='basket-modal' onClick={(ev) => props.closeButton()}>
            <div className='basket-modal__component' onClick={(ev) => ev.stopPropagation()}>
                <div className='basket-modal__info'>
                    <img className='basket-modal__img' src={props.elem.url} alt=""/>
                    <div className='basket-modal__info-about'>
                        <p className='basket-modal__name'>{props.elem.name}</p>
                        <p className='basket-modal__price'>{props.elem.price}<span className='basket-modal__green'> $</span></p>
                    </div>
                    <p className='basket-modal__delete' onClick={() => {
                        if(props.basket ) {
                            props.deleteItem(props.elem)
                        }
                        if(props.favorite) {
                            props.delFav(props.elem)
                        }
                    }}>X</p>
                </div>
            </div>
        </div>

    )
}

BasketModal.propTypes = {
    name: PropTypes.string,
    url: PropTypes.string,
    price: PropTypes.number,
    code: PropTypes.number,
    elem: PropTypes.object
}
BasketModal.defaultProps = {
    name: "Bulbasaur",
    url: "./img/Bulbasaur.png",
    price: 0,
}

export default BasketModal