import React, {useEffect, useState} from "react";
import PropTypes from "prop-types";
import './List.scss'
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import {Star} from "../SVG/Star";


const List = (props) => {
    const [modal, setModal] = useState(false)
    const [color, setColor] = useState('black')

    useEffect(() => {
        setColor('black')
        props.fav.forEach(el => {
            if(el.code === props.list.code) {
                setColor('orange')
            }
        })
    }, [props])


    return (
            <div className='list'>
                <div className='list__item'>
                    <div key={props.id}>
                        <h2 className='list__title-name'>{props.name}</h2>
                        <img className='list__img' src={props.url} alt=""/>
                        <div className='list__svg'>
                            <Star color = {color} size = '20px' clickStar = {() => {
                                    props.favorite(props.list)
                                color === 'black' ? setColor('orange') : setColor('black')

                                }
                            }/>
                        </div>
                    </div>
                    <div>
                        <p className='list__price' >{props.price}<span> $</span></p>
                        <p className='list__code'>{props.code}</p>
                        <div className='list__button'>
                            <Button backgroundColor= '#000000' text='buy' onClick={() =>  setModal(true)}/>
                        </div>
                    </div>
                </div>
                {modal &&
                    <Modal header = 'Do you wanna buy pokemon?'
                           text = 'Press yes,boy!'
                           closeButton = {() => setModal(false)}
                           action = {
                               <>
                                   <Button backgroundColor= 'black'
                                           text='buy'
                                           onClick = {() => {setModal(false)
                                           props.basket(props.list)
                                           }}/>
                                   <Button backgroundColor= 'black'
                                           text='cancel'
                                           onClick = {() => setModal(false)}/></>}
                    />}
            </div>
    )
}

List.propTypes = {
    name: PropTypes.string,
    url: PropTypes.string,
    price: PropTypes.number,
    code: PropTypes.number
}
export default List;