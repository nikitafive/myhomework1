import React from 'react';
import {Link, Outlet} from "react-router-dom";
import './NavMenu.scss'
import {ButtonBack} from "../ButtonBack/ButtonBack";


export function NavMenu () {
    return (
        <>
            <menu className="menu">
                <Link className="menu__item" to="/">Home</Link>
                <Link className="menu__item" to="basket">Basket</Link>
                <Link className="menu__item" to="favorites">Favorites</Link>
                <ButtonBack/>
            </menu>
            <Outlet/>
        </>

    )
}