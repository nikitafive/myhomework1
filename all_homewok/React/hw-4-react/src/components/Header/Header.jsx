import React, {useState} from "react";
import './Header.scss'
import {Star} from "../SVG/Star";
import Basket from "../SVG/Basket";
import Button from "../Button/Button";
import BasketModal from "../BasketModal/BasketModal";


const Header = (props) => {
    const [basket, setBasket] = useState(false)
    const [favorite, setFavorite] = useState(false)
    return (
        <header className='header'>
            <h1 className='header__title'>Pokemons</h1>
            <div className='header__elements'>
                <div className='header__star'>
                    <Star clickStar ={() => {
                        setFavorite(true)
                        setBasket(false)
                    }}/>
                    <span>{props.favorite.length}</span>
                </div>
                <div className='header__basket'>
                    <Basket clickBasket = {() => {
                        setBasket(true)
                        setFavorite(false)
                    }}/>
                    <span>{props.basket.length}</span>
                </div>
            </div>
            {basket &&
                <div className='basket' onClick={(ev) => setBasket(false)}>
                    <div className='basket-modal-component' onClick={(ev) => ev.stopPropagation()}>
                        <p className='basket-modal-close' onClick={() => setBasket(false)}>X</p>
                        <p className='basket-text'>Basket</p>
                        {props.basket.map((elem) => (
                            <BasketModal key={elem.code} elem={elem} deleteItem={props.deleteItem} basket={props.basket}/>
                        ))}
                        <Button backgroundColor = "#000000"
                                text = "Cancel"
                                onClick = {() => setBasket(false)}/>
                    </div>
                </div>}
            {favorite &&
                <div className='basket' onClick={(ev) => setFavorite(false)}>
                    <div className='basket-modal-component' onClick={(ev) => ev.stopPropagation()}>
                        <p className='basket-modal-close' onClick={() => setFavorite(false)}>X</p>
                        <p className='basket-text'>Your favorites</p>
                        {props.favorite.map((elem) => (
                            <BasketModal key={elem.code} elem={elem} delFav={props.delFav} favorite={props.favorite} />
                        ))}
                        <Button backgroundColor = "#000000"
                                text = "Cancel"
                                onClick = {() => setFavorite(false)}/>
                    </div>
                </div>}
            {console.log("heder", props.basket)}
        </header>

    )
}

export default Header