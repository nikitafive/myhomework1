
import React, { useState, useEffect } from 'react';
import List from "../List/List";
import './ListItem.scss'


const ListItem = (props) => {

const [stateItem, setListItem] = useState([])

    useEffect(() => {
    fetch(`./file.json`)
        .then(r => r.json())
        .then(data => {
            setListItem(data)
        })
    })

    return (
        <div>
            <p className='header__text'>Сhoose your pokemon</p>
            <div className='pokemon'>
                {stateItem.map(list =>
                    <List key={list.code}
                          name={list.name}
                          url={list.url}
                          price={list.price}
                          code={list.code}
                          list={list}
                          basket={props.basket}
                          favorite={props.favorite}
                          fav={props.fav}
                    />
                )}
            </div>
        </div>

    )
}

export default ListItem;
