import React from "react";
import './Modal.scss'
import {Button} from "../Button/Button";



const Modal = (props) => {
    return (
        <div className='modal' onClick={(ev) => props.closeButton()}>
            <div className='modal__content'onClick={(ev) => ev.stopPropagation()} >
                <div className='modal__content-title'>
                    <h1 className='modal__content-title-text'>{props.header}</h1>
                    <span className='modal__content-title-close' onClick={() => props.closeButton()}>X</span>
                </div>
                <p className='modal__content-text'>{props.text}</p>
                <div className='modal__button'>
                    {props.action}
                </div>
            </div>
        </div>
    )
}

export default Modal

