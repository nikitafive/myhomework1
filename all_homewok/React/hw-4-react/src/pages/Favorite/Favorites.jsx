import './Favorites.scss'
import {useSelector} from "react-redux";
import {useEffect} from "react";
import {FavoriteItem} from "./FavoriteItem/FavoriteItem";


export function Favorites () {
    const favorite = useSelector(state => state.favorite)
    useEffect(() => {
        localStorage.setItem('favorite',JSON.stringify(favorite))
    },[favorite])

    return (
        <section className="favorite">
            <h2 className="favorite__title">Favorites</h2>
            <div className="favorite__popup">
                {favorite.map((pokemon) => (
                   <FavoriteItem key={pokemon.code} pokemon={pokemon}/>
                ))}
            </div>
        </section>
    )
}