import React, {useEffect, useState} from 'react';
import './Basket.scss'
import {useSelector} from "react-redux";
import {BasketItem} from "./BasketItem/BasketItem";

export function Basket () {
    const basket = useSelector(state => state.basket)
    useEffect(() => {
        localStorage.setItem('basket',JSON.stringify(basket))
    },[basket])

    return (
        <section className="nav-basket">
            <h2 className="nav-basket__title">Basket</h2>
            <div className="nav-basket__popup">
                {basket.map((pokemon) => (
                    <BasketItem key={pokemon.code} pokemon={pokemon}/>
                ))}
            </div>
        </section>
    )
}