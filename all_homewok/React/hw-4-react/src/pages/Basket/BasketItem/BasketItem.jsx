import React from "react";
import {useDispatch, useSelector} from "react-redux";
import './BasketItem.scss'

export function BasketItem (props)  {
    const dispatch = useDispatch()

    return (
        <div className="popup__items" key={props.pokemon.code}>
            <img className="popup__items__img" src={props.pokemon.url} alt=""/>
            <h3 className="popup__items__name">{props.pokemon.name}</h3>
            <p><strong></strong> amount</p>
            <p className="popup__items__text">Price <strong>{props.pokemon.price}</strong> <span>$</span></p>
            <p className="popup__items__delete" onClick={() => {
                dispatch({type:'OPEN_DELETE_BASKET_MODAL', payload:{pokemonCode: props.pokemon.code}})
            }}>X</p>
        </div>
    );
}