import React, {useEffect} from "react";
import ListItem from '../../components/ListItem/ListItem';
import Header from "../../components/Header/Header";
import {useDispatch, useSelector} from "react-redux";
import './Main.scss';

export function Main () {
    const basket = useSelector(state => state.basket)
    const favorite = useSelector(state => state.favorite)
    const dispatch = useDispatch() // это функция в которую мы передаем команды для изменения state который харнится в store
    // она принимает action всегда, она позволяет вызывать reducer

    useEffect(() => {
        localStorage.setItem('basket',JSON.stringify(basket))
        localStorage.setItem('favorite',JSON.stringify(favorite))
    },[basket, favorite])


    function favoriteUser(item) {
        let repetition = false
        favorite.forEach(el => {
            if(el.code === item.code) {
                repetition = true
                dispatch({type: 'REMOVE_PRODUCT_FROM_FAVORITE_ACTION_TYPE', payload:{item}})
            }
        })
        if(!repetition) {
            dispatch({type: 'ADD_PRODUCT_TO_FAVORITE_ACTION_TYPE', payload:{item}})
        }
    }

    return (
        <div className='Main'>
            <Header basket={basket} favorite={favorite}/>
            <ListItem fav={favorite} favorite={favoriteUser}/>
        </div>
    )
    // delFav={favoriteUser}
    // favorite={favoriteUser}
}

