import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import {
    createBrowserRouter,
    RouterProvider,
} from "react-router-dom";
import {NavMenu} from "./components/NavMenu/NavMenu";
import {Basket} from './components/Basket/Basket';
import {Favorites} from "./components/Favorite/Favorites";


const router = createBrowserRouter([
    {
        path: "/",
        element: <NavMenu/>,
        errorElement: <div>Not Found</div>,
        children: [
            {
                index: "/",
                element: <App/>,
            },
            {
                path: "/basket",
                element: <Basket/>,
            },
            {
                path: "/favorites",
                element: <Favorites/>,
            },

        ]
    },
]);


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <RouterProvider router={router} />
);

