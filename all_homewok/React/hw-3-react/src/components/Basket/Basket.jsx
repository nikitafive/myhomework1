import React, {useState} from 'react';
import './Basket.scss'
import {NavMenuItem} from "../NavMenuItem/NavMenuItem";

export function Basket () {
    const [basket, setBasket] = useState(JSON.parse(localStorage.getItem('basket')) || [])

    function delItem(item) {
        basket.forEach(el => {
            if (el.code === item.code) {
                setBasket(basket.filter(el => el.code !== item.code))
                localStorage.setItem('basket', JSON.stringify(basket.filter(el => el.code !== item.code)))
            }
        })
    }

    return (
        <section className="nav-basket">
            <h2 className="nav-basket__title">Basket</h2>
            <div className="nav-basket__popup">
                {basket.map((pokemon) => (
                    <NavMenuItem key={pokemon.code} pokemon={pokemon} del={delItem}/>
                ))}
            </div>
        </section>
    )
}