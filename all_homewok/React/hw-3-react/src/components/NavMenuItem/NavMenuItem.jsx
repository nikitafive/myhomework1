import React, {useState} from "react";
import Modal from "../Modal/Modal";
import Button from "../Button/Button";
import './NavMenuItem.scss'

export function NavMenuItem (props)  {
    const [modal, setModal] = useState(false)
    return (
            <div className="popup__items" key={props.pokemon.code}>
                <img className="popup__items__img" src={props.pokemon.url} alt=""/>
                <h3 className="popup__items__name">{props.pokemon.name}</h3>
                <p><strong></strong> amount</p>
                <p className="popup__items__text">Price <strong>{props.pokemon.price}</strong> <span>$</span></p>
                <p className="popup__items__delete" onClick={() => setModal(true)}>X</p>
                {modal &&
                    <Modal
                        header = "Are you sure you want to delete this item?"
                        text = "item will be removed"
                        closeButton = {() => setModal(false)}
                        action = { <>
                            <Button backgroundColor = "black"
                                    text = "Ok"
                                    onClick = {() => {
                                        props.del(props.pokemon)
                                        setModal(false)

                                    }}/>
                            <Button backgroundColor = "black"
                                    text = "Cancel"
                                    onClick = {() => setModal(false)}/>
                        </>}
                    />
                }
            </div>

    );
};

