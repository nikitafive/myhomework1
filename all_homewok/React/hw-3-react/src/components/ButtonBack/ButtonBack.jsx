import React from "react";
import {useNavigate} from "react-router-dom";
import './ButtonBack.scss'


export function ButtonBack () {
    const navigate = useNavigate();
    return (
        <button className="button-back" onClick={() => {navigate(-1)}}>Back</button>
    )
}
