import React, {useState} from 'react';
import './Favorites.scss'
import {NavMenuItem} from "../NavMenuItem/NavMenuItem";


export function Favorites () {

    const [favorite, setFavorite] = useState(JSON.parse(localStorage.getItem('favorite')) || [])

    function delItem(item) {
        favorite.forEach(el => {
            if (el.code === item.code) {
                setFavorite(favorite.filter(el => el.code !== item.code))
                localStorage.setItem('favorite', JSON.stringify(favorite.filter(el => el.code !== item.code)))
            }
        })
    }

    return (
        <section className="favorite">
            <h2 className="favorite__title">Favorites</h2>
            <div className="favorite__popup">
                {favorite.map((pokemon) => (
                   <NavMenuItem key={pokemon.code} pokemon={pokemon} del={delItem}/>
                ))}
            </div>
        </section>
    )
}