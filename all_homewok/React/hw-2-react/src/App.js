import React from "react";
import './App.scss';
import {Button} from './components/Button/Button';
import {Modal} from './components/Modal/Modal';
import {List} from "./components/List/List";
import {Header} from "./components/Header/Header";
import {Star} from "./components/SVG/Star";
import {ListItem} from "./components/ListItem/ListItem";


export class App extends React.Component {
constructor(props) {
    super(props);
    this.state = {
        basket: JSON.parse(localStorage.getItem('basket')) || [],
        favorite: JSON.parse(localStorage.getItem('favorite')) || []
   }
   this.basketUser = this.basketUser.bind(this)
    this.favorite = this.favorite.bind(this)
}

basketUser (item) {
    this.setState({basket: [...this.state.basket, item]},
        () => localStorage.setItem('basket', JSON.stringify(this.state.basket)))
}

favorite(item) {
    let repetition = false
    this.state.favorite.forEach(el => {
        if(el.code === item.code)
            repetition = true
    })

    if(!repetition) {
        this.setState({favorite: [...this.state.favorite, item]},
            () => localStorage.setItem('favorite', JSON.stringify(this.state.favorite)))
    }

}

    render() {
        return (
            <div className='App'>
                <Header basket = {this.state.basket} favorite={this.state.favorite}/>
               <ListItem basket = {this.basketUser} favorite={this.favorite}/>
            </div>
        )
    }
}
