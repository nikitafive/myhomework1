import React from 'react';
import './BasketModal.scss'
import PropTypes from "prop-types";



export class BasketModal extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className='basket-modal'>
                <div className='basket-modal__component'>
                                 <div className='basket-modal__info'>
                                     <img className='basket-modal__img' src={this.props.elem.url} alt=""/>
                                 <div className='basket-modal__info-about'>
                                     <p className='basket-modal__name'>{this.props.elem.name}</p>
                                     <p className='basket-modal__price'>{this.props.elem.price}<span className='basket-modal__green'> $</span></p>
                                 </div>
                                 </div>
                </div>
            </div>

        )
    }
}


BasketModal.propTypes = {
    name: PropTypes.string,
    url: PropTypes.string,
    price: PropTypes.number,
    code: PropTypes.number,
    elem: PropTypes.object
}
BasketModal.defaultProps = {
    name: "Bulbasaur",
    url: "./img/Bulbasaur.png",
    price: 0,
}
