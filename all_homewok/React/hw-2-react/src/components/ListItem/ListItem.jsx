import React from 'react';
import {List} from "../List/List";
import {Button} from "../Button/Button";
import {Modal} from "../Modal/Modal";
import "./ListItem.scss"

export class ListItem extends React.Component{
constructor(props) {
    super(props);
    this.state = {
        product: []
    }
}

    componentDidMount() {
        fetch(`./file.json`)
            .then(r => r.json())
            .then(data => {
                this.setState({product: data})
            })
    }
    render() {
        return (
            <div>
                <p className='header__text'>Сhoose your pokemon</p>
                <div className='pokemon'>
                    {this.state.product.map((prod) => (
                        <List key={prod.code}
                              name={prod.name}
                              url={prod.url}
                              price={prod.price}
                              code={prod.code}
                              prod={prod}
                              basket={this.props.basket}
                              favorite={this.props.favorite}
                        />
                    ))}

                </div>
            </div>
        )
    }

}
