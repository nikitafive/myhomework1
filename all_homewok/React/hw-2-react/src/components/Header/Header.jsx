import React from "react";
import './Header.scss'
import {Star} from "../SVG/Star";
import {Basket} from "../SVG/Basket";
import {BasketModal} from "../BasicModal/BasketModal";
import {ListItem} from "../ListItem/ListItem";
import {List} from "../List/List";
import PropTypes from "prop-types";
import {Button} from "../Button/Button";

export class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            BasketModal: false,
            favorite:false
        }
    }

    render() {
        return (
            <header className='header'>
            <h1 className='header__title'>Pokemons</h1>
                <div className='header__elements'>
                    <div className='header__star'>
                        <Star clickStar ={() => {
                            this.setState({favorite:true})
                            this.setState({BasketModal:false})
                            console.log('gogi')
                        }}/>
                        <span>{this.props.favorite.length}</span>
                    </div>
                    <div className='header__basket'>
                        <Basket clickBasket = {() => {
                           this.setState({BasketModal:true})
                            this.setState({favorite:false})
                        }}/>
                        <span>{this.props.basket.length}</span>
                    </div>
                </div>
                {this.state.BasketModal &&
                        <div className='basket' onClick={(ev) => this.setState({BasketModal:false})}>
                            <div className='basket-modal-component'>
                                <p className='basket-modal-close' onClick={() => this.setState({BasketModal:false})}>X</p>
                                <p className='basket-text'>Basket</p>
                        {this.props.basket.length > 0 ?
                            this.props.basket.map((elem) => (
                                <BasketModal key={elem.code} elem={elem}/>
                                     )): (<p className='basket-null'>no products</p>)}
                                <Button backgroundColor = "#000000"
                                        text = "Cancel"
                                        ButtonClick = {() => this.setState({BasketModal: false})}/>
                            </div>

                        </div>}

                {this.state.favorite &&
                    <div className='basket' onClick={(ev) => this.setState({favorite:false})}>
                        <div className='basket-modal-component'>
                            <p className='basket-modal-close' onClick={() => this.setState({favorite:false})}>X</p>
                            <p className='basket-text'>Your favorites</p>
                            {this.props.favorite.length > 0 ?
                                this.props.favorite.map((elem) => (
                                <BasketModal key={elem.code} elem={elem}/>
                                    )): (<p className='basket-null'>no products</p>)}
                            <Button backgroundColor = "#000000"
                                    text = "Cancel"
                                    ButtonClick = {() => this.setState({favorite: false})}/>
                        </div>
                    </div>}
            </header>

        )
    }
}