import React from "react";
import PropTypes from 'prop-types'
import './List.scss'
import {Star} from '../SVG/Star'
import {Modal} from "../Modal/Modal";
import {Button} from "../Button/Button";


export class List extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Modal: false,
            defColor: 'black'
        }
    }

    render() {
        return (
            <div className='list'>
                    <div className='list__item'>
                <div key={this.props.id}>
                    <h2 className='list__title-name'>{this.props.name}</h2>
                    <img className='list__img' src={this.props.url} alt=""/>
                    <div className='list__svg'>
                        <Star color = {this.state.defColor} size = '20px' clickStar = {() => {
                            if (this.state.defColor === 'black') {
                                this.setState({defColor: 'orange'})
                                this.props.favorite(this.props.prod)
                            }
                        }}/>
                    </div>
                </div>
                <div>
                    <p className='list__price' >{this.props.price}<span> $</span></p>
                    <p className='list__code'>{this.props.code}</p>
                    <div className='list__button'>
                        <Button backgroundColor= '#000000' text='buy' onClick={() => this.setState({Modal: true})}/>
                    </div>
                </div>
                    </div>
                {this.state.Modal &&
                    <Modal header = 'Do you wanna buy pokemon?'
                           text = 'Press yes,boy!'
                           closeButton = {() => this.setState({Modal: false})}
                           action = {
                               <>
                                   <Button backgroundColor= 'black'
                                           text='buy'
                                           onClick = {() => {
                                               this.props.basket(this.props.prod)
                                               this.setState({Modal: false})
                                           }}/>
                                   <Button backgroundColor= 'black'
                                           text='cancel'
                                           onClick = {() => this.setState({Modal:false})}/></>}
                    />}
            </div>
        )
    }
}

List.propTypes = {
    name: PropTypes.string,
    url: PropTypes.string,
    price: PropTypes.number,
    code: PropTypes.number
}

