import React from "react";
import './Button.scss'

export class Button extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <button className='button' onClick={() => this.props.onClick()} style={{backgroundColor: this.props.backgroundColor}}> {this.props.text} </button>
        )
    }
}

